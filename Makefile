all: main.o estatistica.o unity.o
	gcc main.o estatistica.o unity.o -o estatistica -DUNITY_INCLUDE_DOUBLE -lm

main.o: main.c estatistica.h unity.h unity_internals.h
	gcc -c main.c -DUNITY_INCLUDE_DOUBLE -Wall -O2

estatistica.o: estatistica.c estatistica.h
	gcc -c estatistica.c -DUNITY_INCLUDE_DOUBLE -Wall -O2 -lm

unity.o: unity.c unity.h unity_internals.h
	gcc -c unity.c -DUNITY_INCLUDE_DOUBLE

clean:
	rm -f *~ estatistica *.o
