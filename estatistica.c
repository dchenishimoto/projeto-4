#include "estatistica.h"
#include <math.h>

double media(int n, double *v) {
    if(n == 0)
        return 0;

    double sum = 0;
    for(int i = 0; i < n; i++)
        sum += *(v + i);

    double val = sum / n;
    // corretor de casas decimais
    val = (int)(val * 1000);
    return val / 1000;
}

double desvio(int n, double *v) {
    if( (n == 0) || (n == 1) )
        return 0;

    double med = media(n, v);   //calcula a media dos valores
    double sum = 0;
    for(int i = 0; i < n; i++)
        sum += pow(*(v + i) - med, 2);

    double val = sqrt(sum / (n - 1));
    // corretor de casas decimais
    val = (int)(val * 1000);
    return val / 1000;
}