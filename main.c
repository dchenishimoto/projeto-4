#include "unity.h"
#include "estatistica.h"

void setUp() {
}

void tearDown() {
}

/* Testes da media */
void inteiros_positivos_media() {
  double v[5] = {1, 2, 5, 7, 8};
  TEST_ASSERT_EQUAL_DOUBLE(4.6, media(5, v));
}

void inteiros_negativos_media() {
  double v[5] = {-3, -9, -5, -7, -8};
  TEST_ASSERT_EQUAL_DOUBLE(-6.4, media(5, v));
}

void inteiros_misturado_media() {
  double v[5] = {-1, 4, -5, 0, -8};
  TEST_ASSERT_EQUAL_DOUBLE(-2, media(5, v));
}

void reais_positivos_media() {
  double v[5] = {0.4, 1.3, 4.5, 7.2, 8.98};
  TEST_ASSERT_EQUAL_DOUBLE(4.476, media(5, v));
}

void reais_negativos_media() {
  double v[5] = {-3.45, -9.2, -5.1, -7, -8.32};
  TEST_ASSERT_EQUAL_DOUBLE(-6.614, media(5, v));
}

void reais_misturado_media() {
  double v[6] = {-1.23, 4.12, -5.11, 1.54, -8.23, 0.2};
  TEST_ASSERT_EQUAL_DOUBLE(-1.451, media(6, v));
}

void vetor_vazio_media() {
  double v[0];
  TEST_ASSERT_EQUAL_DOUBLE(0, media(0, v));
}
/*-----------------------------------------------------*/
/* Testes do desvio padrao */
void inteiros_positivos_desvio() {
  double v[5] = {1, 2, 5, 7, 8};
  TEST_ASSERT_EQUAL_DOUBLE(3.049, desvio(5, v));
}

void inteiros_negativos_desvio() {
  double v[5] = {-3, -9, -5, -7, -8};
  TEST_ASSERT_EQUAL_DOUBLE(2.408, desvio(5, v));
}

void inteiros_misturado_desvio() {
  double v[5] = {-1, 4, -5, 0, -8};
  TEST_ASSERT_EQUAL_DOUBLE(4.636, desvio(5, v));
}

void reais_positivos_desvio() {
  double v[5] = {0.4, 1.3, 4.5, 7.2, 8.98};
  TEST_ASSERT_EQUAL_DOUBLE(3.688, desvio(5, v));
}

void reais_negativos_desvio() {
  double v[5] = {-3.45, -9.2, -5.1, -7, -8.32};
  TEST_ASSERT_EQUAL_DOUBLE(2.347, desvio(5, v));
}

void reais_misturado_desvio() {
  double v[6] = {-1.23, 4.12, -5.11, 1.54, -8.23, 0.2};
  TEST_ASSERT_EQUAL_DOUBLE(4.518, desvio(6, v));
}

void vetor_vazio_desvio() {
  double v[0];
  TEST_ASSERT_EQUAL_DOUBLE(0, desvio(0, v));
}

void um_elemento_desvio() {
  double v[1] = {1};
  TEST_ASSERT_EQUAL_DOUBLE(0, desvio(1, v));
}
/*-----------------------------------------------------*/

int main() {
  UNITY_BEGIN();

  // Testes da media
  printf(">> Testes da media:\n");
  RUN_TEST(inteiros_positivos_media);
  RUN_TEST(inteiros_negativos_media);
  RUN_TEST(inteiros_misturado_media);
  RUN_TEST(reais_positivos_media);
  RUN_TEST(reais_negativos_media);
  RUN_TEST(reais_misturado_media);
  RUN_TEST(vetor_vazio_media);

  // Testes do desviao padrao
  printf("\n>> Testes do desviao padrao:\n");
  RUN_TEST(inteiros_positivos_desvio);
  RUN_TEST(inteiros_negativos_desvio);
  RUN_TEST(inteiros_misturado_desvio);
  RUN_TEST(reais_positivos_desvio);
  RUN_TEST(reais_negativos_desvio);
  RUN_TEST(reais_misturado_desvio);
  RUN_TEST(vetor_vazio_desvio);
  RUN_TEST(um_elemento_desvio);

  return UNITY_END();
}
